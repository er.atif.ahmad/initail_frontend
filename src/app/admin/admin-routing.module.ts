import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';

const routes: Routes = [
  { path: '', component: AdminLayoutComponent, 
    children: [
      { path: '', redirectTo: 'dashboard/home', pathMatch: 'full' },
      { path: 'dashboard', redirectTo: 'dashboard/home', pathMatch: 'full' },
      { path: 'dashboard/home', component: DashboardComponent },
      {
        path: 'access-controls/users', loadChildren: () => import('./components/users/users.module').then(m => m.UsersModule),
        data: {preload: false}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
