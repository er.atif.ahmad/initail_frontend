import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SharedModule } from '../shared/shared.module';
import { UiModule } from '../ui/ui.module';
import { NotificationComponent } from './components/notification/notification.component';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [
    DashboardComponent,
    AdminLayoutComponent,
    SidebarComponent,
    NotificationComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    TranslocoModule,
    SharedModule,
    UiModule
  ]
})
export class AdminModule { }
