import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/core/services/helper.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss', '../../../common-scss/common.scss']
})
export class AddUserComponent implements OnInit {

  urlPrefix;
  addUserForm: FormGroup;
  userId;
  userDetails;
  loaded :boolean = false;

  constructor(private helper: HelperService, private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);
    
    this.createFilterForm();

    this.route.params.subscribe(v =>{
      if(v.userId){
        this.userId = v.userId;
        this.getUserDetails();
      }
    });
  }

  createFilterForm(){
    this.addUserForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('',[Validators.email, Validators.required]),
      userName: new FormControl('',[Validators.required]),
      mobileNo: new FormControl('',[Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      password: new FormControl('',[Validators.required])
    });
  }


  getUserDetails(){
    this.userService.getUserDetailById(this.userId).subscribe(res =>{
      console.log("==== Recieved From API ===", res);
      this.userDetails = res.data;
      
      console.log("==== Recieved From API ===", this.userDetails);
      this.loaded = true;
      this.setData();
    }, err => this.helper.showRelaventErrorPopup(err));
  }


  setData(){
    if(this.userDetails){
      this.addUserForm.patchValue(
        {
          name: this.userDetails.name,
          email: this.userDetails.email,
          userName: this.userDetails.userName,
          mobileNo: this.userDetails.mobileNo,
          password: this.userDetails.password
        }
      );
    }
  }

  onSubmit(){
    console.log(this.addUserForm.value);
    if(this.addUserForm.valid){
      let data = this.addUserForm.value;
      if(this.userId){
        delete data.password;
        this.userService.updateUser(data, this.userId).subscribe(res =>{
          console.log("=== user updated ===", res);
          this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users/details/${this.userId}`);
        }, err => this.helper.showRelaventErrorPopup(err));
      }
      else{
        this.userService.createUser(this.addUserForm.value).subscribe(res =>{
          console.log("=== user created ===", res);
          this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users`);
        }, err => this.helper.showRelaventErrorPopup(err));
      }
      
    }
    else{
      this.helper.showRelaventSuccessPopup('Fill all the required fields', 'Error', 3000);
    }
  }


  back(){
    if(this.userId){
      this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users/details/${this.userId}`);
    }
    else{
      this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users`);
    }
  }

}
