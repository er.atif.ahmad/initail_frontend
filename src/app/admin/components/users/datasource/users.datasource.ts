import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { UserService } from '../services/user.service';

export class UsersDataSource implements DataSource<any> {

    private dataSubject = new BehaviorSubject<any[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    public totalDataLength = new BehaviorSubject<number>(0);

    constructor(private userService: UserService) { }

    connect(collectionViewer: CollectionViewer): Observable<any[]> {
        return this.dataSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dataSubject.complete();
        this.loadingSubject.complete();
    }

    load(pageIndex: number, pageSize: number, filter?: object, sortFilter?: object) {
        const page = pageIndex + 1;
        this.loadingSubject.next(true);

        this.userService.getUsers(page, pageSize, filter, sortFilter).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe(r => {
            console.log('DataSource: Reply:', r);
            this.totalDataLength.next(r.total);
            const rows = r.data;
            this.dataSubject.next(rows);
        });
    }
}
