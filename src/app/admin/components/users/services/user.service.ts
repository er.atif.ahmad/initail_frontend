import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }


  getUsers(currentPage = 1, limit = 10, filters?: object, sortFilter?: object): Observable<any> {
    const body = JSON.stringify({ ...filters, ...sortFilter });
    return this.http.post(`/api/v1/admin/access-control/users?current_page=${currentPage}&per_page=${limit}`,body);
  }


  getUserDetailById(id): Observable<any> {
    return this.http.get(`/api/v1/admin/access-control/users/${id}`);
  }

  createUser(data): Observable<any> {
    return this.http.post(`/api/v1/admin/access-control/users/create`, data);
  }

  updateUser(data, userId): Observable<any> {
    return this.http.put(`/api/v1/admin/access-control/users/${userId}`, data);
  }

  updateUserStatus(data, userId): Observable<any> {
    return this.http.patch(`/api/v1/admin/access-control/users/${userId}`, data);
  }

}
