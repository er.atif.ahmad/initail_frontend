import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { finalize, tap } from 'rxjs/operators';
import { UsersDataSource } from '../datasource/users.datasource';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss', '../../../common-scss/common.scss']
})

export class UserListComponent implements OnInit {

  urlPrefix;

  isFilter = false;
  dataSource: UsersDataSource;
  filterForm:FormGroup;
  columns = ['name', 'email', 'userName', 'mobileNo', 'status', 'dateCreated', 'action'];
  displayedColumns: string[] = this.columns;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('columnSelect') columnSelect: MatSelect;

  constructor(private router:Router, private snackbar: MatSnackBar, private userService: UserService) { }

  ngOnInit(): void {

    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);
    
    this.createFilterForm();
    this.dataSource = new UsersDataSource(this.userService)
    this.dataSource.load(0, 10);

    console.log(this.dataSource);
    
  }  

  ngAfterViewInit() {
    setTimeout(() => {
      this.paginator.page.pipe(
        tap(() => this.getUsers())
      ).subscribe();
    }, 100);
  }

  getUsers(filter = '') {
    //console.log(filter);
    
    if (filter) {
      this.paginator.pageIndex = 0;
      this.dataSource.load(0, this.paginator.pageSize, {filter}, null);
    } else {
      this.dataSource.load(this.paginator.pageIndex, this.paginator.pageSize, null, null);
    }
  }

  openSelectColumns() {
    this.columnSelect.open();
  }

  details(id){
    this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users/details/${id}`);
  }


  updateStatus(element){
    console.log("==== element ====", element);
    let userId = element.id;
    let data = {isActive:element.isActive};
    this.userService.updateUserStatus(data, userId).pipe(
      finalize(() => {
        console.log("=== updating ===");
      })
    ).subscribe(r => {
      console.log('Update Status:', r.data);
      this.snackbar.open(r.message, 'Info', {
        duration: 3000
      });
      //this.router.navigateByUrl('/admin/access-control/manage-users');
    });
  }

  createFilterForm(){
    this.filterForm = new FormGroup({
      name: new FormControl(''),
      email: new FormControl('',[Validators.email])
    });
  }

  filterServiceRequest() {

    console.log('Filter form submited...');
    console.log(this.filterForm);
    if(!this.filterForm.invalid){
      this.getUsers(this.filterForm.value);
    }
    
  }

  resetFilter() {
    this.dataSource.load(0, this.paginator.pageSize, null, null);
  }

}
