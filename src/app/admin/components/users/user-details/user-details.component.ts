import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/core/services/helper.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss', '../../../common-scss/common.scss']
})
export class UserDetailsComponent implements OnInit {

  urlPrefix;
  userId;
  loaded :boolean = false;
  userDetails;

  constructor(private helper: HelperService, private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);

    this.route.params.subscribe(v =>{
      if(v.userId){
        this.userId = v.userId;
        this.getUserDetails();
      }
    });
  }

  getUserDetails(){
    this.userService.getUserDetailById(this.userId).subscribe(res =>{
      console.log("==== Recieved From API ===", res);
      this.userDetails = res.data;
      
      console.log("==== Recieved From API ===", this.userDetails);
      this.loaded = true;
    }, err => this.helper.showRelaventErrorPopup(err));
  }

  editUserDetails(){
    this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/users/edit-user/${this.userId}`);
  }

}
