import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss', '../../common-scss/common.scss']
})
export class NotificationComponent implements OnInit {

  @Input() isShow = true;
  @Output() onToggle = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  
  toggleSidevar(){
    this.isShow = !this.isShow;
    this.onToggle.emit(this.isShow)
  }

}
