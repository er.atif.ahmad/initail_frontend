import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { environment as env } from "src/environments/environment";
import { LoginComponent } from "./component/login/login.component";
import { AdminGuard } from "./core/guards/admin.guard";
import { SuperadminGuard } from "./core/guards/superadmin.guard";



const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path : 'login', component: LoginComponent, data: { showHeader: false, showFooter: false} },
    { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), canActivate: [AdminGuard], data: { preload: false,  showHeader: false, showFooter: false } },
    { path: 'superadmin', loadChildren: () => import('./superadmin/superadmin.module').then(m => m.SuperadminModule), canActivate: [SuperadminGuard], data: { preload: false,  showHeader: false, showFooter: false } }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
      useHash: false
      // preloadingStrategy: env.production || env.stage ? PreloadSelectedModules : null
      // preloadingStrategy: PreloadSelectedModules
      ,
      relativeLinkResolution: 'legacy'
    }
    )],
    exports: [RouterModule],
    providers: [],
    // providers: [PreloadSelectedModules],
  })
  export class AppRoutingModule { }
  