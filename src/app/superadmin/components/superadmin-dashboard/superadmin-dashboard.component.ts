import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-superadmin-dashboard',
  templateUrl: './superadmin-dashboard.component.html',
  styleUrls: ['./superadmin-dashboard.component.scss', '../../common-scss/common.scss']
})
export class SuperAdminDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
