import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class RoleService {

  constructor(private http: HttpClient) { }

  getAllResources(): Observable<any> {
    return this.http.get(`/api/v1/superadmin/access-control/roles/resources`);
  }

  // getRoles(): Observable<any> {
  //   return this.http.get(`/api/v1/superadmin/access-control/roles`);
  // }

  getRoles(currentPage = 1, limit = 10, filters?: object, sortFilter?: object): Observable<any> {
    const body = JSON.stringify({ ...filters, ...sortFilter });
    return this.http.post(`/api/v1/superadmin/access-control/role?current_page=${currentPage}&per_page=${limit}`,body);
  }


  getRoleDetailById(id): Observable<any> {
    return this.http.get(`/api/v1/superadmin/access-control/role/${id}`);
  }

  createRole(data): Observable<any> {
    return this.http.post(`/api/v1/superadmin/access-control/role/create`, data);
  }

  updateRole(data, roleId): Observable<any> {
    return this.http.put(`/api/v1/superadmin/access-control/role/${roleId}`, data);
  }

  updateRoleStatus(data, userId): Observable<any> {
    return this.http.patch(`/api/v1/superadmin/access-control/role/${userId}`, data);
  }

}
