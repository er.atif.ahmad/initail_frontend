import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/core/services/helper.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss', '../../../common-scss/common.scss']
})
export class AddRoleComponent implements OnInit {

  urlPrefix;
  addRoleForm: FormGroup;
  roleId;
  userDetails;
  loaded :boolean = false;
  languages:any = [];

  constructor(private helper: HelperService, private fb:FormBuilder, private roleService: RoleService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);
    
    this.createFilterForm();

    this.getAllLanguage();

    this.route.params.subscribe(v =>{
      if(v.roleId){
        this.roleId = v.roleId;
        setTimeout(() => {
          this.getRoleDetails();
        }, 1000);
      }
    });
  }

  getAllLanguage(){
    this.helper.getAllLanguage().subscribe(res =>{
      console.log("==== Recieved From API ===", res);

      this.languages = res.data;

      this.languages.map(e =>{
        this.addName({codeKey: e.codeKey, value: '', codeValue: e.codeValue});
      });

    }, err => this.helper.showRelaventErrorPopup(err));
  }

  createFilterForm(){
    this.addRoleForm = this.fb.group({
      names: this.fb.array([]),
      code: new FormControl('', Validators.required),
      isActive: new FormControl(true, Validators.required),
    });
  }

  names() : FormArray {
    return this.addRoleForm.get("names") as FormArray
  }
   
  newName(obj): FormGroup {
    return this.fb.group({
      codeKey: new FormControl(obj?.codeKey, Validators.required),
      value: new FormControl(obj?.value, Validators.required),
      codeValue: new FormControl(obj?.codeValue, Validators.required)
    })
  }
   
  addName(data) {
    this.names().push(this.newName(data));
  }

  updateName(index, data){
    (this.names().at(index) as FormGroup).patchValue(data);
  }
   
  removeName(i:number) {
    this.names().removeAt(i);
  }


  getRoleDetails(){
    this.roleService.getRoleDetailById(this.roleId).subscribe(res =>{
      console.log("==== Recieved From API ===", res);
      this.userDetails = res.data;
      
      console.log("==== Recieved From API ===", this.userDetails);
      this.loaded = true;
      
      this.setData();

      let names = JSON.parse(this.userDetails.name);
      
      this.languages.map((e, i) =>{
        this.updateName(i, {codeKey: e.codeKey, value: names[e.codeKey], codeValue: e.codeValue});
      });

    }, err => this.helper.showRelaventErrorPopup(err));
  }


  setData(){
    if(this.userDetails){
      this.addRoleForm.patchValue(
        {
          code: this.userDetails.code,
          isActive: Boolean(this.userDetails.isActive),
        }
      );
    }
  }

  onSubmit(){
    console.log(this.addRoleForm.value);

    if(this.addRoleForm.valid){
      let data = this.addRoleForm.value;
      let tempName = {};
      
      data.names.map(e =>{
        console.log(e);
        
        tempName[e.codeKey] = e.value;
        console.log(tempName);
        
      });
      
      data.name = JSON.stringify(tempName);
      delete data.names;

      console.log(data);
      if(this.roleId){
        this.roleService.updateRole(data, this.roleId).subscribe(res =>{
          console.log("=== role updated ===", res);
          this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles`);
        }, err => this.helper.showRelaventErrorPopup(err));
      }
      else{
        this.roleService.createRole(data).subscribe(res =>{
          console.log("=== role created ===", res);
          this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles`);
        }, err => this.helper.showRelaventErrorPopup(err));
      }
      
    }
    else{
      this.helper.showRelaventSuccessPopup('Fill all the required fields', 'Error', 3000);
    }
  }


  back(){
    if(this.roleId){
      this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles`);
    }
    else{
      this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles`);
    }
  }

}
