import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { finalize, tap } from 'rxjs/operators';
import { RolesDataSource } from '../datasource/roles.datasource';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss', '../../../common-scss/common.scss']
})
export class RoleListComponent implements OnInit {

  urlPrefix;

  isFilter = false;
  dataSource: RolesDataSource;
  filterForm:FormGroup;
  columns = ['name', 'code', 'status', 'dateCreated','active', 'action'];
  displayedColumns: string[] = this.columns;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('columnSelect') columnSelect: MatSelect;

  constructor(private router:Router, private snackbar: MatSnackBar, private roleService: RoleService) { }

  ngOnInit(): void {

    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);
    
    this.createFilterForm();
    this.dataSource = new RolesDataSource(this.roleService)
    this.dataSource.load(0, 10);

    console.log(this.dataSource);
    
  }  

  ngAfterViewInit() {
    setTimeout(() => {
      this.paginator.page.pipe(
        tap(() => this.getRoles())
      ).subscribe();
    }, 100);
  }

  getRoles(filter = '') {
    //console.log(filter);
    
    if (filter) {
      this.paginator.pageIndex = 0;
      this.dataSource.load(0, this.paginator.pageSize, {filter}, null);
    } else {
      this.dataSource.load(this.paginator.pageIndex, this.paginator.pageSize, null, null);
    }
  }

  openSelectColumns() {
    this.columnSelect.open();
  }

  details(id){
    this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles/details/${id}`);
  }


  updateStatus(element){
    console.log("==== element ====", element);
    let userId = element.id;
    let data = {isActive:element.isActive};
    this.roleService.updateRoleStatus(data, userId).pipe(
      finalize(() => {
        console.log("=== updating ===");
      })
    ).subscribe(r => {
      console.log('Update Status:', r.data);
      this.snackbar.open(r.message, 'Info', {
        duration: 3000
      });
      //this.router.navigateByUrl('/admin/access-control/manage-users');
    });
  }

  createFilterForm(){
    this.filterForm = new FormGroup({
      code: new FormControl('', [Validators.required])
    });
  }

  filterServiceRequest() {

    console.log('Filter form submited...');
    console.log(this.filterForm);
    if(!this.filterForm.invalid){
      this.getRoles(this.filterForm.value);
    }
    
  }

  resetFilter() {
    this.dataSource.load(0, this.paginator.pageSize, null, null);
  }

  jsonParse(jsonString){
    return JSON.parse(jsonString);
  }

  editRoleDetails(roleId){
    this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles/edit-role/${roleId}`);
  }

}
