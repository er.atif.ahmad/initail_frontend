import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RoleListComponent } from './role-list/role-list.component';
import { AddRoleComponent } from './add-role/add-role.component';
import { RoleDetailsComponent } from './role-details/role-details.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UiModule } from 'src/app/ui/ui.module';
import { RoleService } from './services/role.service';
import { TranslocoModule } from '@ngneat/transloco';


@NgModule({
  declarations: [
    RoleListComponent,
    AddRoleComponent,
    RoleDetailsComponent
  ],
  imports: [
    CommonModule,
    RolesRoutingModule,
    SharedModule,
    UiModule,
    TranslocoModule,
  ],
  providers:[RoleService]
})
export class RolesModule { }
