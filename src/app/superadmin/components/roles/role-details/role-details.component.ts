import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/core/services/helper.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-role-details',
  templateUrl: './role-details.component.html',
  styleUrls: ['./role-details.component.scss', '../../../common-scss/common.scss']
})
export class RoleDetailsComponent implements OnInit {

  urlPrefix;
  roleId;
  loaded :boolean = false;
  roleDetails;

  constructor(private helper: HelperService, private roleService: RoleService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);

    this.route.params.subscribe(v =>{
      if(v.roleId){
        this.roleId = v.roleId;
        this.getRoleDetails();
      }
    });
  }

  getRoleDetails(){
    this.roleService.getRoleDetailById(this.roleId).subscribe(res =>{
      console.log("==== Recieved From API ===", res);
      this.roleDetails = res.data;
      
      console.log("==== Recieved From API ===", this.roleDetails);
      this.loaded = true;
    }, err => this.helper.showRelaventErrorPopup(err));
  }

  editRoleDetails(){
    this.router.navigateByUrl(`/${this.urlPrefix}/access-controls/roles/edit-role/${this.roleId}`);
  }

}
