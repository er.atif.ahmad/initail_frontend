import { EventEmitter, Input } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-superadmin-sidebar',
  templateUrl: './superadmin-sidebar.component.html',
  styleUrls: ['./superadmin-sidebar.component.scss', '../../common-scss/common.scss']
})
export class SuperAdminSidebarComponent implements OnInit {
  @Input() isShow = true;
  @Output() onToggle = new EventEmitter();

  urlPrefix;

  constructor() { }

  ngOnInit(): void {
    let url = location.pathname;
    console.log("==== url =====", url);

    this.urlPrefix = url.split('/')[1];
    console.log("==== urlPrefix ====",this.urlPrefix);
    
    
  }
  
  toggleSidevar(){
    this.isShow = !this.isShow;
    this.onToggle.emit(this.isShow)
  }

  isActiveExpension(nav1 = true, nav2 = true){
    if(nav1 || nav2){
      return true;
    }
    else{
      return false;
    }
  }

}
