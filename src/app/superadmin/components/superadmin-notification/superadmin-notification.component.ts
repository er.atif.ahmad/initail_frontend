import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-superadmin-notification',
  templateUrl: './superadmin-notification.component.html',
  styleUrls: ['./superadmin-notification.component.scss', '../../common-scss/common.scss']
})
export class SuperAdminNotificationComponent implements OnInit {

  @Input() isShow = true;
  @Output() onToggle = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  
  toggleSidevar(){
    this.isShow = !this.isShow;
    this.onToggle.emit(this.isShow)
  }

}
