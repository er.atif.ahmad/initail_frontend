import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { HelperService } from 'src/app/core/services/helper.service';

@Component({
  selector: 'app-superadmin-layout',
  templateUrl: './superadmin-layout.component.html',
  styleUrls: ['./superadmin-layout.component.scss']
})
export class SuperAdminLayoutComponent implements OnInit {

  @ViewChild('menuTrigger') menuTrigger: MatMenuTrigger;

  hasBackdropSidebar = false;
  modeSidebar = "push";
  isOpenedSidebar: boolean = false;
  
  hasBackdropNotification = false;
  modeNotification = "over";
  isOpenedNotification: boolean = false;
  
  isMobile: boolean = true;

  languages;

  year = new Date().getFullYear();


  isHeadset$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Handset, Breakpoints.TabletPortrait])
  .pipe(
    map(result => { console.log("==== result ===", result); 
      return result.matches;
    }),
    shareReplay()
  );


  constructor(private translocoService: TranslocoService, private helper: HelperService, private authService: AuthService, private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.isHeadset$.subscribe(r =>{
      console.log("IS MOBILE:", r);
      this.isMobile = r;
      this.isOpenedSidebar = !this.isMobile;
      this.hasBackdropSidebar = this.isMobile;
      if(this.isMobile){
        this.modeSidebar = "over";
      }
    });

    this.getAllLanguage();
  }

  logout(){
    this.authService.logout();
  }

  changeActiveLanguage(langCode: string) {
    this.translocoService.setActiveLang(langCode);
  }

  getAllLanguage(){
    this.helper.getAllLanguage().subscribe(res =>{
      console.log("==== Recieved From API ===", res);

      this.languages = res.data;

    }, err => this.helper.showRelaventErrorPopup(err));
  }

}
