import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuperAdminDashboardComponent } from './components/superadmin-dashboard/superadmin-dashboard.component';
import { SuperAdminLayoutComponent } from './components/superadmin-layout/superadmin-layout.component';

const routes: Routes = [{ path: '', component: SuperAdminLayoutComponent, 
children: [
  { path: '', redirectTo: 'dashboard/home', pathMatch: 'full' },
  { path: 'dashboard', redirectTo: 'dashboard/home', pathMatch: 'full' },
  { path: 'dashboard/home', component: SuperAdminDashboardComponent },
  {
    path: 'access-controls/roles', loadChildren: () => import('./components/roles/roles.module').then(m => m.RolesModule),
    data: {preload: false}
  }
]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperadminRoutingModule { }
