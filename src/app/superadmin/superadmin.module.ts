import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SuperadminRoutingModule } from './superadmin-routing.module';
import { UiModule } from '../ui/ui.module';
import { SharedModule } from '../shared/shared.module';
import { TranslocoModule } from '@ngneat/transloco';
import { SuperAdminDashboardComponent } from './components/superadmin-dashboard/superadmin-dashboard.component';
import { SuperAdminLayoutComponent } from './components/superadmin-layout/superadmin-layout.component';
import { SuperAdminNotificationComponent } from './components/superadmin-notification/superadmin-notification.component';
import { SuperAdminSidebarComponent } from './components/superadmin-sidebar/superadmin-sidebar.component';


@NgModule({
  declarations: [
    SuperAdminDashboardComponent,
    SuperAdminLayoutComponent,
    SuperAdminNotificationComponent,
    SuperAdminSidebarComponent
  ],
  imports: [
    CommonModule,
    SuperadminRoutingModule,
    TranslocoModule,
    SharedModule,
    UiModule
  ]
})
export class SuperadminModule { }
