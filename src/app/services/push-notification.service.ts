import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SwPush } from '@angular/service-worker';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationService {

  constructor(private http: HttpClient,
    private swPush: SwPush,
    private appEnv: AppConfigService, ) { }

  init() {
    if (this.swPush.isEnabled) {

      this.swPush.subscription.subscribe(r => {
        if (!r) {
          console.log('Client Not Subscribed For Push: Requesting');
          this.swPush
            .requestSubscription({
              serverPublicKey: this.appEnv.getConfig().vapidKey,
            })
            .then(subscription => {
              console.log('Send Subscription to the server:', subscription);
              this.sendPushSubscription(subscription).subscribe(r => {
                console.log('Send Push Subscription to Backend Success: Response', r);
                localStorage.setItem('push-subscription', JSON.stringify(r));
              });
            })
            .catch(console.error);
        } else {
          console.log('Already Subscribed to push Notifications:', r);
        }
      });

      this.swPush.messages.subscribe(r => {
        console.log('Push Notification Received: ', r);
      });

      // Subscribe for User Action Clicks
      this.swPush.notificationClicks.subscribe(r => {
        console.log('Push Notification Clicked:', r);
        console.log('Push Notification Clicked:', JSON.stringify(r));
        if (r.action && r.action === 'explore' && r.notification.data.url) {
          window.focus();
          window.open(r.notification.data.url, '_self');
        }
      });
    }
  }

  private sendPushSubscription(subscription: PushSubscription): Observable<any> {
    return this.http.post('/push-notification/subscribe', subscription);
  }

}
