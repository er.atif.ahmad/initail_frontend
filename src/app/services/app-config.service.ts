import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

@Injectable()
export class AppConfigService {
  private appConfig: CONFIG;

  loadAppConfig() {
    return new Promise((resolve, reject) => {

      let configUrl = `/assets/data/config/dev/appConfig.json`;

      if (env.production) {
        configUrl = `/assets/data/config/production/appConfig.json`;
      }

      if (env.stage) {
        configUrl = `/assets/data/config/stage/appConfig.json`;
      }

      fetch(configUrl).then(response => {
        if (response.status !== 200) {
          console.log('Looks like there was a problem in Fetching AppConfig. Status Code: ' +
            response.status);
          reject();
        }
        // Examine the text in the response
        response.json().then(data => {
          this.appConfig = data;
          if (env.production) {
            console.log('App Config Initialized.');
          } else {
            console.log('App Config Initialized with data:', this.appConfig);
          }
          resolve(true);
        });
      }).catch(function (err) {
        console.log('AppConfig Fetch Error :', err);
        reject();
      });
    });
  }

  getConfig(): CONFIG {
    return this.appConfig;
  }

}

export interface CONFIG {
  appName: string;
  origin: string;
  socketOrigin: string;
  bucketUrl: string;
  cdnUrl: string;
  appUrl: string;
  apiKey: string;
  vapidKey: string;
}
