import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private userThemeConfig = null;
  userThemeConfigData = new ReplaySubject<any>(1);

  private orgId = null;
  orgIdData = new ReplaySubject<any>(1);

  private headerShown = true;
  private footerShown = true;
  sideBarState = new Subject<boolean>();
  navBarStatus = new Subject<boolean>();

  private shouldShowNavBarContents = false;

  constructor(private http: HttpClient) { }

  showNavBar() {
    this.shouldShowNavBarContents = true;
    this.navBarStatus.next(this.shouldShowNavBarContents);
  }

  hideNavBar() {
    this.shouldShowNavBarContents = false;
    this.navBarStatus.next(this.shouldShowNavBarContents);
  }

  showSideBar() {
    this.sideBarState.next(true);
  }

  hideSideBar() {
    this.sideBarState.next(false);
  }

  setDefaultHeaderAndFooterState() {
    this.footerShown = true;
    this.headerShown = true;
  }

  isFooterShown() {
    return this.footerShown;
  }

  isHeaderShown() {
    return this.headerShown;
  }

  hideFooter() {
    this.footerShown = false;
  }

  showFooter() {
    this.footerShown = true;
  }

  hideHeader() {
    this.headerShown = false;
  }

  showHeader() {
    this.headerShown = true;
  }


  public setUserThemeConfig(theme) {
    this.userThemeConfig = theme;
    this.userThemeConfigData.next(this.userThemeConfig);
  }

  public setOrgId(orgId) {
    this.orgId = orgId;
    this.orgIdData.next(this.orgId);
  }

  /* GET ORGANISATION DETAILS*/
  getShopThemeConfig(value, name = 'id'): Observable<any> {
    return this.http.get(
      `/administration-features/organisations/get-organisation-details-theme?${name}=${value}`
    );
  }

}
