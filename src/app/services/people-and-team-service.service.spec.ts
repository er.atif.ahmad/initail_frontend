import { TestBed } from '@angular/core/testing';

import { PeopleAndTeamServiceService } from './people-and-team-service.service';

describe('PeopleAndTeamServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeopleAndTeamServiceService = TestBed.get(PeopleAndTeamServiceService);
    expect(service).toBeTruthy();
  });
});
