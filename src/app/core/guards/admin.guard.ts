import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from '@angular/router';
import { Store } from '@ngrx/store';
import { take, map } from 'rxjs/operators';
import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as fromApp from 'src/app/core/app.states';
import * as AuthActions from 'src/app/core/store/actions/auth.actions';
import { Observable } from 'rxjs';


@Injectable()
export class AdminGuard implements CanActivate, CanLoad {
  constructor(private router: Router, private store: Store<fromApp.AppState>) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('auth').pipe(
      take(1),
      map((authState: fromAuth.State) => {
        console.log("=== authState ===", authState);
        
        if (authState.isAuthenticated && (authState.user.isShopAdmin || authState.user.isShopUser)) {
          return true;
        } else {
          this.store.dispatch(AuthActions.unAuthorized());
        }
      })
    );
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select('auth').pipe(
      take(1),
      map((authState: fromAuth.State) => {
        console.log("=== authState ===", authState);
        if (authState.isAuthenticated && (authState.user.isShopAdmin || authState.user.isShopUser)) {
          return true;
        } else {
          return false;
        }
      })
    );
  }
}