import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromApp from 'src/app/core/app.states';
import { map, take } from 'rxjs/operators';
import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as AuthActions from 'src/app/core/store/actions/auth.actions';

@Injectable({
  providedIn: 'root'
})
export class SuperadminGuard implements CanActivate, CanLoad {
  constructor(private router: Router, private store: Store<fromApp.AppState>) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return this.store.select('auth').pipe(
      take(1),
      map((authState: fromAuth.State) => {
        if (authState.isAuthenticated && authState.user.isSuperAdmin) {
          return true;
        } else {
          this.store.dispatch(AuthActions.unAuthorized());
        }
      })
    );
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select('auth').pipe(
      take(1),
      map((authState: fromAuth.State) => {
        if (authState.isAuthenticated && authState.user.isSuperAdmin) {
          return true;
        } else {
          return false;
        }
      })
    );
  }
}
