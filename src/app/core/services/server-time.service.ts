import { Injectable } from '@angular/core';
import { ReplaySubject, Subscription, timer, throwError } from 'rxjs';
import { HelperService } from './helper.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { SkipAuthInterceptorHeader } from '../interceptors/authorize-request-interceptor';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ServerTimeService {
  syncInterval = 1000 * 3 * 60; // Every Three Minutes

  localInitializationTime: number; // milliseconds ticks
  timeSyncDiff: number;

  ticks: number;
  currentServerTime = new ReplaySubject(1);

  sub: Subscription;
  timerStarted = false;

  constructor(private http: HttpClient, private helper: HelperService) { }

  public init() {
    const currentLocalTime = this.helper.getTimeStamp();
    this.currentServerTime.next(currentLocalTime);
    this.localInitializationTime = currentLocalTime;
    this.syncServerTime();
  }

  private startTimer() {
    this.timerStarted = true;
    const timers = timer(1, 1000);
    this.sub = timers.subscribe(t => {
      // const nTime = new Date().getTime();
      const nTime = this.helper.getTimeStamp();
      this.ticks = nTime - this.timeSyncDiff;
      this.currentServerTime.next(this.ticks);

      const durationExpired = Math.floor((nTime - this.localInitializationTime) / 1000);
      if (durationExpired > 1 && durationExpired % (this.syncInterval / 1000) === 0) {
        this.syncServerTime();
      }
    });
  }

  private syncServerTime() {
    this.getServerTime().subscribe((res: any) => {
      const localTime = this.helper.getTimeStamp();
      const serverTime = res.data.systemTime;
      console.log('Server Time:', serverTime);
      console.log('Local Time:', localTime);
      this.timeSyncDiff = localTime - serverTime;
      if (!this.timerStarted) {
        this.startTimer();
        // this.logServerTime();
      }
    });
  }

  private logServerTime() {
    setInterval(() => { }, 10 * 1000);
  }

  private getServerTime() {
    const url = '/api/v1/utility/get-system-time';
    const headers = new HttpHeaders().set(SkipAuthInterceptorHeader, '');
    return this.http.get(url, {headers}).pipe(catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
