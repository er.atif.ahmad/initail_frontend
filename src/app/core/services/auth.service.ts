import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { LoginResponse } from 'src/app/shared/interfaces/login-response';
import { AppConfigService } from 'src/app/services/app-config.service';

import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as fromApp from 'src/app/core/app.states';
import { Store } from '@ngrx/store';
import * as AuthActions from 'src/app/core/store/actions/auth.actions';
import { Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  testUser = { email: 'user@email.com', password: '1234', token: 'sampleToken' };

  constructor(public appEnv: AppConfigService, protected http: HttpClient, private store: Store<fromApp.AppState>) { }


  isLoggedIn() {
    const token = sessionStorage.getItem('accessToken');
    return token != null;
  }

  login(email: string, password: string): Observable<any> {
    // this is a mocked response to be able to test the example
    // console.log('Email:', email, 'Password:', password);
    const body = JSON.stringify({ shopName: email, password, })// apiKey: this.appEnv.getConfig().apiKey });

    return this.http.post<LoginResponse>('/api/v1/entrance/login', body);
  }

  logout() {
    this.store.dispatch(AuthActions.logout());
    console.log('**************LogOut Called**********************');
  }

  refreshAccessToken(): Observable<any> {
    let body = JSON.stringify({ refreshToken: '' });
    return this.store.select('auth').pipe(
      take(1),
      switchMap((authState: fromAuth.State) => {
        if (authState && authState.refreshToken) {
          body = JSON.stringify({ refreshToken: authState.refreshToken });
          return this.http.post<LoginResponse>('/api/v1/token/refresh-access-token', body);
        } else {
          return this.http.post<LoginResponse>('/api/v1/token/refresh-access-token', body);
        }
      })
    );
  }

  verifyUser(data): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post<LoginResponse>('/api/v1/user/verify-and-update', body);
  }

  recoverPassword(data): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post('/api/v1/entrance/recover-password', body);
  }

  verifyForgotPassword(data): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post('/api/v1/entrance/verify-update-password', body);
  }

  updateProfile(data): Observable<any> {
    const body = JSON.stringify(data);
    return this.http.post('/api/v1/user/update-profile', body);
  }

  signup(data): Observable<any> {
    return this.http.post('/signup/create-user', JSON.stringify(data))
  }

  /*FORGOT PASSWORD */
  forgotPassword(data): Observable<any> {
    return this.http.post('/signup/forgot-password', JSON.stringify(data))
  }

  /*RESET PASSWORD */
  resetPassword(data): Observable<any> {
    return this.http.post('/signup/reset-password', JSON.stringify(data))
  }

  /*VERIFY EMAIL */
  verifyEmail(data): Observable<any> {
    return this.http.get(`/signup/verify-account/${data}`)
  }

  /*REJECT EMAIL */
  rejectEmail(data): Observable<any> {
    return this.http.get(`/signup/reject-account/${data}`)
  }

  authorizeLineAccount(data): Observable<any> {
    data = JSON.stringify(data);
    return this.http.post(`/entrance/authorize-line-account`, data)
  }

  removeLineAccount(): Observable<any> {
    return this.http.get(`/entrance/remove-line-account`);
  }
  //GET USER DETAILS
  getUserDetails(data): Observable<any> {
    return this.http.get(`/signup/get-user-details?id=${data.id}`);
  }

}
