import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { CheckForUpdateService } from './check-for-update.service';
import { LogUpdateService } from './log-update.service';
import { PromptUpdateService } from './prompt-update.service';

@Injectable({
    providedIn: 'root'
})
export class ServiceWorkerService {

    constructor(
        private checkForUpdateService: CheckForUpdateService,
        private logUpdateService: LogUpdateService,
        private promptUpdateService: PromptUpdateService
    ) { }


    // Init All ServiceWorker Related Services
    public initServiceWorkerServices() {
        console.log('*******************SWWWWWWW******************');

        console.log('Starting Service Worker Services');
        this.checkForUpdateService.init();
        this.logUpdateService.init();
        this.promptUpdateService.init();

        // Register To Push Messages
        console.log('*******************SWWWWWWW******************');
    }

}
