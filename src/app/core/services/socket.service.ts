import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { AppConfigService } from 'src/app/services/app-config.service';
import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as fromApp from 'src/app/core/app.states';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import FingerprintJS from '@fingerprintjs/fingerprintjs';


export interface ChannelStream {
  channelName: string;
  responseCount: number,
  closeAfterFirstResponse: boolean,
  data$: BehaviorSubject<any>;
}



@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private endPoint = '';
  private socket = null;

  private fp = null;
  private browserId = null;

  public socketConnectionStatus = new BehaviorSubject<Boolean>(false);

  private channels: ChannelStream[] = [];

  constructor(private appEnv: AppConfigService,
    private store: Store<fromApp.AppState>) {
  }

  public init() {
    this.endPoint = this.appEnv.getConfig().socketOrigin;
    console.log('WS Endpoint:', this.endPoint);

    let accessToken = '';

    this.store.select('auth').subscribe(async (authState: fromAuth.State) => {
      console.log('Auth Token Changed: Reconnecting with Socket');

      // We recommend to call `load` at application startup.
      if (!this.fp) {
        this.fp = await FingerprintJS.load();
        // The FingerprintJS agent is ready.
      }

      // Get a visitor identifier when you'd like to.
      const result = await this.fp.get();

      // This is the visitor identifier:
      this.browserId = result.visitorId;
      console.log('Unique Browser Id:', this.browserId);

      if (authState.isAuthenticated) {
        accessToken = authState.accessToken;
        this.endPoint += '?Auth=' + accessToken + '&deviceId=' + this.browserId;
        if (this.socket) {
          this.socket.close();
          this.socketConnect();
        } else {
          this.socketConnect();
        }
      }
    });
  }

  private socketConnect() {
    this.socket = new WebSocket(this.endPoint);

    this.socket.onclose = ({ wasClean, code, reason }) => {
      console.log('Socket connection Closed:', { wasClean, code, reason });
      this.socketConnectionStatus.next(false);
    };

    this.socket.onerror = error => {
      console.log('Socket connection Error:', error);
    };

    this.socket.onmessage = ({ data }) => {
      console.log('Socket connection New Message:', data);
      data = JSON.parse(data);
      this.broadcastToChannel(data.channel, data);
    };

    this.socket.onopen = () => {
      console.log('Socket connection Established:');
      this.socketConnectionStatus.next(true);

      // Try sending initial test message...
      this.socket.send(
        JSON.stringify({ action: 'welcome', data: { message: 'This is initial test message... from' + this.browserId } })
      );

    };

  }

  public getSocketIoObject() {
    return this.socket;   // Can be used for requests and listening events
  }

  public callApiViaSocket(url, body = null) {
    let socketBodyPayload = {
      route: url, body
    };
    this.socket.send(
      // This message will be routed to 'api' based on the 'action'
      // property
      JSON.stringify({ action: 'api', data: socketBodyPayload })
    );
    let channel = this.subscribeToChannel(url, true);
    return channel.data$;
  }

  public subscribeToChannel(channelName, closeChannelAfterFirstResponse = false) {
    let channel = this.channels.find(ch => ch.channelName == channelName);
    if (!channel) {
      let bhs = new BehaviorSubject(0);
      channel = { channelName, data$: bhs, responseCount: 0, closeAfterFirstResponse: closeChannelAfterFirstResponse };
      this.channels.push(channel);
    }

    // Adding test subscriber for the channel...
    channel.data$.subscribe(r => {
      console.log('[TEST-SUBSCRIBER-SOCKET-DATA]:', r);
    });

    return channel;
  }

  private broadcastToChannel(channelName, data) {
    let channel = this.channels.find(ch => ch.channelName == channelName);
    if (!channel) {
      console.log('Socket - Local Broadcast channel not available, may be no one is looking... and hence no channel created');
      return;
    }
    channel.data$.next(data);
    console.log(`Socket - Local Broadcasted data on channel "${channel.channelName}": `, data);

    channel.responseCount++;

    if (channel.closeAfterFirstResponse && channel.responseCount > 0) {
      console.log(`Closing channel ${channel.channelName}: `, channel);
      channel.data$.complete();
      this.channels = this.channels.filter(ch => ch.channelName !== channelName);
    }

  }

}
