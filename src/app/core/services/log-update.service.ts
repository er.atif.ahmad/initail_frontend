import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
    providedIn: 'root'
})
export class LogUpdateService {

    constructor(private updates: SwUpdate) { }

    public init() {
        this.updates.available.subscribe(event => {
            console.log('App Current Version :', event.current);
            console.log('App Available Version:', event.available);
        });
        this.updates.activated.subscribe(event => {
            console.log('App Old Version was:', event.previous);
            console.log('App New Version is:', event.current);
        });
        console.log('Subscribed To Log Update Service.');
    }
}
