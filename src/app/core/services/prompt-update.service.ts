import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { VersionPopupComponent } from 'src/app/core/components/version-popup/version-popup.component';
import { MatSnackBar } from '@angular/material/snack-bar';


@Injectable({
    providedIn: 'root'
})
export class PromptUpdateService {

    constructor(private updates: SwUpdate,private snackBar : MatSnackBar) { }

    public init() {
        this.updates.available.subscribe(event => {
            this.checkIfReloadNeeded(event.available);
        });
        console.log('Subscribed To Prompt Update Service.');
    }

    private checkIfReloadNeeded(available) {
        console.log('New App Version is available. App Data:', available.appData);
        if (available.appData.forceReload) {


            this.snackBar.openFromComponent(VersionPopupComponent,{
                verticalPosition: 'top',
                horizontalPosition: 'right',
                duration: 0,
              })

            // if (confirm('New Version Of ServiceMind App is available. Click "Ok" to reload your browser tab to start using new version.')) {
            //     console.log('Reload');
            //     window.location.reload();
            // } else {
            //     console.log('Reload on Update Canceled');
            // }
        } else {
            console.log('Force Reload is not enabled.');
        }
    }
}
