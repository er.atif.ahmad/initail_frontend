import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';
import { environment as env, environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class CheckForUpdateService {

    constructor(private appRef: ApplicationRef, private updates: SwUpdate) { }

    public init() {

        if (env.stage || env.production) {
            // Allow the app to stabilize first, before starting polling for updates with `interval()`.
            const appIsStable$ = this.appRef.isStable.pipe(first(isStable => isStable === true));
            const everyHours$ = interval(1 * 3 * 60 * 1000);
            const everyHourOnceAppIsStable$ = concat(appIsStable$, everyHours$);

            everyHourOnceAppIsStable$.subscribe(() => {
                this.updates.checkForUpdate();
                console.log('Checked For Version Update. (NGSW)');
            });
            console.log('Subscribed To Check For Updates Service.(NGSW)');
        } else {
            console.log('Skipping service worker update check in local...')
        }
    }
}
