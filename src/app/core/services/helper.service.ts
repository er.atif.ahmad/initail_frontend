import { Injectable, HostListener, Inject, Directive } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { environment as env } from 'src/environments/environment';
import { WINDOW } from './window-ref.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SkipAuthInterceptorHeader } from '../interceptors/authorize-request-interceptor';
import { NgxImageCompressService } from 'ngx-image-compress';

@Directive()
@Injectable()
export class HelperService {

  constructor(
    @Inject(WINDOW) private window: Window,
    private router: Router,
    private http: HttpClient,
    private appConfigService: AppConfigService,
    private imageCompress: NgxImageCompressService,
    private snackBar: MatSnackBar
  ) { }

  insertIntoArray(array: any[], item: any, index?: number): any[] {
    if (index) {
      if (index === 0) {
        array.unshift(item);
      } else {
        array.splice(index, 0, item);
      }
    } else {
      array.push(item);
    }
    return array;
  }


  removeFromArray(array: any[], item: any): any[] {
    const index = array.indexOf(item);
    array.splice(index, 1);
    return array;
  }

  currency(amount) {
    if (amount >= 1000) {
      let amt = amount.toString();
      let amtArray = amt.split(".");
      //console.log(amtArray);
      let finalAmount = amtArray[0];
      let am = '';
      let indx = (finalAmount.length - 3);
      for (let index = finalAmount.length; index > 0; index--) {
        //console.log("index = ",index);

        if (index == indx) {

          if (index == 3) {
            am = ',' + am;
            indx = -1;
          }
          else {
            //console.log("indx = ",indx);
            am = ',' + am;
            indx = indx - 2;
          }

        }
        //console.log((index - 1), finalAmount[(index - 1)]);

        am = finalAmount[(index - 1)] + am;

      }

      if (amtArray[1]) {
        am = am + '.' + amtArray[1];
      }
      //console.log(amt.length);

      //console.log("a-amount",am);
      return am;
    }
    else {
      //console.log("b-amount",amount);

      return amount;
    }
  }

  pushIfNotExists(arr: any[], attr: string, obj: any) {
    const index = arr.find(s => s[attr] === obj[attr]);
    if (!index) {
      arr.push(obj);
    }
    return arr;
  }

  // pushIfNotExist(arr, attr, obj) {
  //   arr.filter(f => f.some(s => s[attr] === obj[attr])).concat([obj]);

  //   // let i = arr.length;
  //   // let flag = true;
  //   // while (i--) {
  //   //   if (arr[i] && arr[i].hasOwnProperty(attr)) {
  //   //     arr[i][attr] = value;
  //   //     flag = false;
  //   //   }
  //   // }
  //   // if (flag) {
  //   //   const obj: any = {};
  //   //   obj[attr] = value;
  //   //   arr.push(obj);
  //   // }
  //   return arr;
  // }

  removeByAttr(arr, attr, value) {
    let i = arr.length;
    while (i--) {
      if (
        arr[i] &&
        arr[i].hasOwnProperty(attr) &&
        (arguments.length > 2 && arr[i][attr] === value)
      ) {
        arr.splice(i, 1);
      }
    }
    return arr;
  }

  returnByAttr(arr, attr, value) {
    return arr.find(item => {
      return item[attr] == value;
    });
  }

  returnManyByAttr(arr, attr, value): any[] {
    return arr.filter(item => {
      return item[attr] == value;
    });
  }

  returnIndexByAttr(arr, attr, value): number {
    const found = arr.find(item => {
      return item[attr] == value;
    });
    return arr.indexOf(found);
  }

  removeWhoHasAttr(arr, attr) {
    let i = arr.length;
    while (i--) {
      if (arr[i] && arr[i].hasOwnProperty(attr) && arguments.length > 1) {
        arr.splice(i, 1);
      }
    }
    return arr;
  }

  returnWhoHasAttr(arr, attr) {
    const temp = new Array();
    let i = arr.length;
    while (i--) {
      if (arr[i] && arr[i].hasOwnProperty(attr)) {
        temp.push(arr[i]);
      }
    }
    return temp;
  }

  validateURL(url: string) {
    const pattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    return url && url.length > 0 && pattern.test(url);
  }

  validateEmail(email: string) {
    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email && email.length > 0 && pattern.test(email);
  }

  validateMobileNumber(mobileNumber) {
    const pattern = /^(\+\d{1,3}|0)?[6-9]\d{9}$/;
    return (
      mobileNumber && mobileNumber.length > 0 && pattern.test(mobileNumber)
    );
  }

  checkIfFileExtensionIsValid(
    filename: string,
    validExtensions?: string[]
  ): boolean {
    if (!validExtensions) {
      validExtensions = ['jpg', 'jpeg', 'gif', 'bmp', 'png'];
    }
    const parts = filename.split('.');
    const extension = parts[parts.length - 1];
    if (validExtensions.indexOf(extension.toLowerCase()) > -1) {
      return true;
    }
    return false;
  }


  checkImageAspectRatio(
    file: File,
    aspectRatio = 4 / 3,
    matchStrictly = false
  ): Promise<boolean> {
    return new Promise(resolve => {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const img = new Image();
        img.src = e.target.result;
        img.onload = (x: any) => {
          const r = img.width / img.height;
          console.log('Required Aspect Ratio: ', aspectRatio);
          console.log('Aspect Ratio: ', r);
          if (matchStrictly) {
            resolve(r === aspectRatio);
          } else {
            resolve(r >= aspectRatio);
          }
        };
      };
      reader.readAsDataURL(file);
    });
  }

  getTimeStamp(date?) {
    if (date) {
      return moment(date).unix() * 1000;
    } else {
      return moment().unix() * 1000;
    }
  }

  getDateObject(date?) {
    if (date) {
      return moment(date);
    } else {
      return moment();
    }
  }

  isValidDate(date) {
    return moment(date).isValid();
  }

  openInNewTab(url, focus?: boolean) {
    if (window.innerWidth < 768 || !env.production) {
      this.router.navigateByUrl(url);
    } else {
      const win = window.open(url, '_blank');
      if (focus) {
        win.focus();
      }
    }
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize() {
    if (this.window.innerWidth < 768) {
      return '_self';
    } else {
      return '_blank';
    }
  }

  openExternalLink(url, focus?: boolean) {
    url = this.getFullUrl(url);
    const win = window.open(url, '_blank');
    console.log('Opened Link: ' + url);
    if (focus) {
      win.focus();
    }
  }

  getFullUrl(url: string): string {
    if (url) {
      if (url.indexOf('http://') === 0 || url.indexOf('https://') === 0) {
        return url;
      } else {
        return 'http://' + url;
      }
    } else {
      return '';
    }
  }

  csvToArray(strData, strDelimiter?) {
    strDelimiter = strDelimiter || ',';

    const objPattern = new RegExp(
      '(\\' +
      strDelimiter +
      '|\\r?\\n|\\r|^)' +
      '(?:"([^"]*(?:""[^"]*)*)"|' +
      '([^"\\' +
      strDelimiter +
      '\\r\\n]*))',
      'gi'
    );

    const arrData = [[]];
    let arrMatches = null;

    while ((arrMatches = objPattern.exec(strData))) {
      const strMatchedDelimiter = arrMatches[1];
      if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter) {
        arrData.push([]);
      }
      let strMatchedValue = '';
      if (arrMatches[2]) {
        strMatchedValue = arrMatches[2].replace(new RegExp('""', 'g'), '"');
      } else {
        strMatchedValue = arrMatches[3];
      }
      arrData[arrData.length - 1].push(strMatchedValue);
    }
    // Return the parsed data.
    return arrData;
  }

  normalizeNumber(val: number, max: number, min = 0) {
    return (val - min) / (max - min);
  }

  generateUniqueId() {
    const len = 7;
    return Math.random()
      .toString(35)
      .substr(2, len);
  }

  getOrdinal(num: number) {
    console.log(num);
    const ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
    if (num % 100 >= 11 && num % 100 <= 13) {
      return num + 'th';
    } else {
      return num + ends[num % 10];
    }
  }

  randomString(limit = 6) {
    const selectString = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < limit; i++) {
      result += selectString[Math.floor(Math.random() * selectString.length)];
    }
    return result;
  }


  genCharArray(charA, charZ) {
    var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
      a.push(String.fromCharCode(i));
    }
    return a;
  }

  fixHeader(data) {
    let arrayToObject = []
    console.log('Import Data: ', data)
    let header = this.genCharArray('A', 'Z');
    for (let i = 0; i < data.length; i++) {
      let finalObj = {}
      for (let j = 0; j < data[i].length; j++) {
        if (!data[i][j]) {
          finalObj[header[j]] = ''
        } else {
          finalObj[header[j]] = data[i][j];
        }
      }
      arrayToObject.push(finalObj)
    }
    return arrayToObject
  }


  cdnUrl(originalUrl: string) {
    const config = this.appConfigService.getConfig();
    console.log('Url (BC):', originalUrl);
    console.log('Bucket Url:', config.bucketUrl);
    if (env.production) {
      if (originalUrl.includes(config.bucketUrl)) {
        originalUrl = originalUrl.replace(config.bucketUrl, config.cdnUrl);
      }
      console.log('Replaced Url (AC):', originalUrl);
    } else {
      console.log('Not changing url, b/c env is not production:', originalUrl);
    }
    return originalUrl;
  }

  async compressFile(image, mimeType): Promise<Blob> {

    let imageData = image;
    let imgResultBeforeCompress: string;
    let imgResultAfterCompress: string;

    let orientation = -1;
    imgResultBeforeCompress = image;
    // console.warn('Image Result (BC):', imgResultBeforeCompress);

    let sizeOfOriginalImage = this.imageCompress.byteCount(image);
    console.warn('Size in KB (Before Compression):', (sizeOfOriginalImage / 1024));

    let compressWidthRatio = 50;
    let compressHeightRatio = 50;

    if ((sizeOfOriginalImage / 1024) < 200) {
      compressWidthRatio = 90;
      compressHeightRatio = 90;
    } else if ((sizeOfOriginalImage / 1024) < 300) {
      compressWidthRatio = 85;
      compressHeightRatio = 85;
    } else if ((sizeOfOriginalImage / 1024) < 400) {
      compressWidthRatio = 80;
      compressHeightRatio = 80;
    } else if ((sizeOfOriginalImage / 1024) < 500) {
      compressWidthRatio = 75;
      compressHeightRatio = 75;
    } else if ((sizeOfOriginalImage / 1024) < 750) {
      compressWidthRatio = 70;
      compressHeightRatio = 70;
    } else if ((sizeOfOriginalImage / 1024) < 1024) {
      compressWidthRatio = 65;
      compressHeightRatio = 65;
    } else {
      compressWidthRatio = 50;
      compressHeightRatio = 50;
    }

    console.log(`Setting compress ratio to compressWidthRatio: ${compressWidthRatio}, compressHeightRatio: ${compressHeightRatio}`);

    try {

      let result = await this.imageCompress.compressFile(image, orientation, compressWidthRatio, compressHeightRatio);

      imgResultAfterCompress = result;
      // console.warn('Image Result (AC):', imgResultAfterCompress);

      let sizeOFCompressedImage = this.imageCompress.byteCount(result)
      console.warn('Size in KB (After Compression):', sizeOFCompressedImage / 1024);

      imageData = result;

    } catch (err) {
      console.log('Image Compress Error:', err);
    }

    let imageBlob: Blob = this.dataURItoBlob(imageData.split(',')[1], mimeType);
    return imageBlob;
  }

  getUploadUrl(filename, mimeType, type): Observable<any> {
    return this.http.post('/users/servicerequest/upload-image-url', { filename, mimeType, type });
  }

  putImage(url, file): Observable<any> {
    const headers = new HttpHeaders({ 'content-type': 'binary/octet-stream' }).set(SkipAuthInterceptorHeader, '');
    return this.http.put(url, file, { headers });
  }

  uploadImageByEntity(data): Observable<any> {
    return this.http.post(
      "/image/upload-image-by-entity",
      JSON.stringify(data)
    );
  }


  dataURItoBlob(dataURI, mimeType): Blob {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: mimeType });
    return blob;
  }


  showRelaventErrorPopup(httpErr: any, duration = 3000) {
    // console.log('TTT:', httpErr?.error);
    const err = httpErr?.error?.errors;
    console.log('Relavent Error:', err);
    if (Array.isArray(err) && err.length > 0) {
      this.snackBar.open(err[0].message, 'Error', {
        duration: duration,
      });
    } else if (httpErr?.error.code) {
      this.snackBar.open(httpErr?.error.message, 'Error', {
        duration: duration,
      });
    } else {
      this.snackBar.open('Oops! Some Error occurred.', 'Error', {
        duration: duration,
      });
    }
  }

  showRelaventSuccessPopup(message: any, typeText = 'done', duration = 3000) {
    // console.log('TTT:', httpErr?.error);
    this.snackBar.open(message, typeText, {
      duration: duration,
    });
  }

  getAllLanguage(currentPage = 1, limit = 100, filters?: object, sortFilter?: object): Observable<any> {
    const body = JSON.stringify({ ...filters, ...sortFilter });
    return this.http.post(`/api/v1/superadmin/master/language?current_page=${currentPage}&per_page=${limit}`,body);
  }

}
