// import { User } from 'src/app/shared/models/user.model';

// import { AuthActionTypes, AuthActions } from 'src/app/core/store/actions/auth.actions';

// export interface State {
//   isAuthenticated: boolean;
//   user: User | null;
//   accessToken: string | null;
//   refreshToken: string | null;
//   errorMessage: string | null;
// }

// // set the initial state swith sessionStorage
// export const initialState: State = {
//   isAuthenticated: sessionStorage.getItem('accessToken') !== null && sessionStorage.getItem('user') != null,
//   accessToken: sessionStorage.getItem('accessToken'),
//   refreshToken: sessionStorage.getItem('refreshToken'),
//   user: JSON.parse(sessionStorage.getItem('user')),
//   errorMessage: null
// };

// export function authReducer(state = initialState, action: AuthActions): State {
//   switch (action.type) {
//     case AuthActionTypes.LOGIN_SUCCESS: {
//       return {
//         ...state,
//         isAuthenticated: true,
//         accessToken: action.payload.accessToken,
//         refreshToken: action.payload.refreshToken,
//         user: action.payload.user,
//         errorMessage: null
//       };
//     }
//     case AuthActionTypes.LOGIN_FAILURE: {
//       let errorMessage = 'Wrong credentials.';
//       const err = action.payload.error;
//       console.log(err);
//       if (Array.isArray(err) && err.length > 0) {
//         errorMessage = err[0].message;
//       }

//       return {
//         ...state,
//         errorMessage
//       };
//     }
//     case AuthActionTypes.LOGOUT: {
//       return {
//         isAuthenticated: false,
//         user: null,
//         accessToken: null,
//         refreshToken: null,
//         errorMessage: null
//       };
//     }
//     default: {
//       return state;
//     }
//   }
// }





import { User } from 'src/app/shared/models/user.model';
import { createReducer, on, Action } from '@ngrx/store';

import * as AuthActions from 'src/app/core/store/actions/auth.actions';
import * as storageHelper from 'src/app/core/helpers/storage-helper';


export interface State {
  isAuthenticated: boolean;
  user: User | null;
  accessToken: string | null;
  refreshToken: string | null;
  errorMessage: string | null;
}

// set the initial state with localStorage
export const initialState: State = {
  isAuthenticated: storageHelper.getFromLocalStorageWithExpiry('accessToken') !== null && storageHelper.getFromLocalStorageWithExpiry('user') != null,
  accessToken: storageHelper.getFromLocalStorageWithExpiry('accessToken'),
  refreshToken: storageHelper.getFromLocalStorageWithExpiry('refreshToken'),
  user: storageHelper.getFromLocalStorageWithExpiry('user'),
  errorMessage: null
};

const authReducer = createReducer(
  initialState,
  on(AuthActions.loginSuccess, (state, { accessToken, refreshToken, user }) => ({
    ...state,
    isAuthenticated: true,
    accessToken,
    refreshToken,
    user,
    errorMessage: null
  })),

  on(AuthActions.refreshTokenSuccess, (state, { accessToken, refreshToken, user }) => ({
    ...state,
    isAuthenticated: true,
    accessToken,
    refreshToken,
    user,
    errorMessage: null
  })),

  on(AuthActions.loginFailure, (state, { error }) => {

    let errorMessage = 'Wrong credentials.';
    const err = error;
    console.log('LOGIN_FAILURE:', err);
    if (Array.isArray(err) && err.length > 0) {
      if (err[0].message) {
        errorMessage = err[0].message;
      }
    }

    return {
      ...state,
      errorMessage
    };
  }),

  on(AuthActions.logout, state => ({
    isAuthenticated: false,
    user: null,
    accessToken: null,
    refreshToken: null,
    errorMessage: null
  })),


  on(AuthActions.updateUser, (state, { user }) => ({
    ...state,
    user,
  }))

);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}
