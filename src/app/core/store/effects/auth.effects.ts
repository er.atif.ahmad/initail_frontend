import { Actions, ofType, createEffect } from '@ngrx/effects';
import { AuthService } from 'src/app/core/services/auth.service';
import { map, switchMap, catchError, tap, mergeMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Injectable, Inject } from '@angular/core';
import { LoginResponse } from 'src/app/shared/interfaces/login-response';
import { WINDOW } from 'src/app/core/services/window-ref.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import * as AuthActions from 'src/app/core/store/actions/auth.actions';

import { Store } from '@ngrx/store';
import * as fromApp from 'src/app/core/app.states';
import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';


import * as storageHelper from 'src/app/core/helpers/storage-helper';
import { of } from 'rxjs';


@Injectable()
export class AuthEffects {

  constructor(private actions$: Actions,
    private authService: AuthService,
    private appConfig: AppConfigService,
    private store: Store<fromApp.AppState>,
    private router: Router, @Inject(WINDOW) private window: Window) { }

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      switchMap(payload => this.authService.login(payload.email, payload.password).pipe(
        map((loginResponse: LoginResponse) => {
          console.log('loginResponse:', loginResponse);
          return AuthActions.loginSuccess(loginResponse);
        }),
        catchError(err => of(AuthActions.loginFailure({ error: err.error.errors })))
      ))
    )
  );


  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      tap((loginResponse: LoginResponse) => {
        console.log('Login_Success [Effect]: payload', loginResponse);
        storageHelper.setToLocalStorageWithExpiry('accessToken', loginResponse.accessToken);
        storageHelper.setToLocalStorageWithExpiry('refreshToken', loginResponse.refreshToken);
        storageHelper.setToLocalStorageWithExpiry('user', loginResponse.user);

        let redirectUrl = new URL(this.window.location.href).searchParams.get('redirectUrl');
        redirectUrl = decodeURI(redirectUrl);
        console.log('Redirect Url********:', redirectUrl);

        if (redirectUrl && redirectUrl != null && redirectUrl != 'null' && redirectUrl != '') {
          this.router.navigateByUrl(redirectUrl);
        } else {
          console.log('Redirecting To Dashboard as we don\'t have redirectUrl');
          if (loginResponse.user.isSuperAdmin) {
            // this.router.navigate(['/admin/administration-features/organisations']);
            this.router.navigate(['/superadmin/dashboard'])
          } else if (loginResponse.user.isCustomer) {
            this.router.navigate(['/user/dashboard']);
          } else {
            this.router.navigate(['/admin/dashboard']);
          }
        }
      }),
    ),
    { dispatch: false }
  );

  refreshTokenSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.refreshTokenSuccess),
      tap((loginResponse: LoginResponse) => {
        console.log('Refresh_Token_Success [Effect]: payload', loginResponse);
        storageHelper.setToLocalStorageWithExpiry('accessToken', loginResponse.accessToken);
        storageHelper.setToLocalStorageWithExpiry('refreshToken', loginResponse.refreshToken);
        storageHelper.setToLocalStorageWithExpiry('user', loginResponse.user);
        // Don't navigate anywhere...
      }),
    ),
    { dispatch: false }
  );

  unAuthorized$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.unAuthorized),
      tap(() => {
        console.log('Unauthorized [Effect] Will redirect to login, with currentUrl as Query Params');
        localStorage.clear();

        let currentUrl = this.window.location.pathname;
        console.log('URL Before Redirect to Login:', currentUrl);
        currentUrl = encodeURI(currentUrl);
        console.log('URL Encoded:', currentUrl);
        const loginUrl = '/login?redirectUrl=' + currentUrl;

        this.router.navigateByUrl(loginUrl);
      }),
    ),
    { dispatch: false }
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      tap(() => {
        console.log(`Logout [Effect] Will redirect to login`);
        storageHelper.removeMultipleKeys(['user', 'accessToken', 'refreshToken']);
        this.router.navigateByUrl('/login');
      }),
    ),
    { dispatch: false }
  );
}

