// import { Action } from '@ngrx/store';

// export enum AuthActionTypes {
//   LOGIN = '[Authentication] Login',
//   LOGIN_SUCCESS = '[Authentication] Login Success',
//   REFRESH_TOKEN_SUCCESS = '[Authentication] Refresh Token Success',
//   LOGIN_FAILURE = '[Authentication] Login Failure',
//   LOGOUT = '[Authentication] Logout',
//   UNAUTHORIZED = '[Authentication] UnAuthorized',
// }

// export class Login implements Action {
//   readonly type = AuthActionTypes.LOGIN;
//   constructor(public payload: any) {}
// }

// export class LoginSuccess implements Action {
//   readonly type = AuthActionTypes.LOGIN_SUCCESS;
//   constructor(public payload: any) {}
// }

// export class RefreshTokenSuccess implements Action {
//   readonly type = AuthActionTypes.REFRESH_TOKEN_SUCCESS;
//   constructor(public payload: any) {}
// }

// export class LoginFailure implements Action {
//   readonly type = AuthActionTypes.LOGIN_FAILURE;
//   constructor(public payload: any) {}
// }

// export class Logout implements Action {
//   readonly type = AuthActionTypes.LOGOUT;
// }

// export class UnAuthorized implements Action {
//   readonly type = AuthActionTypes.UNAUTHORIZED;
//   // constructor(public payload: any) {}
// }

// export type AuthActions =
//   | Login
//   | LoginSuccess
//   | RefreshTokenSuccess
//   | LoginFailure
//   | Logout
//   | UnAuthorized;




import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/shared/models/user.model';

export const login = createAction('[Authentication] Login',
  props<{ email: string; password: string }>()
);

export const loginSuccess = createAction('[Authentication] Login Success',
  props<{ accessToken: string, refreshToken: string, user: User }>()
);

export const refreshTokenSuccess = createAction('[Authentication] Refresh Token Success',
  props<{ accessToken: string, refreshToken: string, user: User }>()
);

export const loginFailure = createAction('[Authentication] Login Failure',
  props<{ error: any }>()
);

export const logout = createAction('[Authentication] Logout');

export const unAuthorized = createAction('[Authentication] UnAuthorized');

export const updateUser = createAction('[Authentication] Update Logged In User',
  props<{ user: User }>()
);