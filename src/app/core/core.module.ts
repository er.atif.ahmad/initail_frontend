import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from 'src/app/core/interceptors';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers } from 'src/app/core/app.states';
import { AuthEffects } from './store/effects/auth.effects';
import { AuthGuard } from './guards/auth.guard';
import { environment as env } from 'src/environments/environment';
import { InternetCheckComponent } from './components/internet-check/internet-check.component';
import { AdminGuard } from './guards/admin.guard';
import { ServerTimeService } from './services/server-time.service';
import { HelperService } from './services/helper.service';
import { WINDOW_PROVIDERS } from './services/window-ref.service';
import { UserGuard } from './guards/user.guard';
import { VersionPopupComponent } from './components/version-popup/version-popup.component';
import { SuperadminGuard } from './guards/superadmin.guard';


@NgModule({
  declarations: [
    InternetCheckComponent,
    VersionPopupComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AuthEffects]),
    !env.production ? StoreDevtoolsModule.instrument({ maxAge: 100 }) : [],
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    UserGuard,
    SuperadminGuard,
    httpInterceptorProviders,
    ServerTimeService,
    HelperService,
    WINDOW_PROVIDERS,
  ],
  exports: [
    InternetCheckComponent,
    VersionPopupComponent
  ]
})
export class CoreModule {
  /* make sure CoreModule is imported only by one NgModule the AppModule */
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
