import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VersionPopupComponent } from './version-popup.component';

describe('VersionPopupComponent', () => {
  let component: VersionPopupComponent;
  let fixture: ComponentFixture<VersionPopupComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
