import {Component, HostListener, OnInit} from '@angular/core';
import {Subscription, of, timer, Observable} from 'rxjs';

@Component({
  selector: 'olympus-internet-check',
  templateUrl: './internet-check.component.html',
  styleUrls: ['./internet-check.component.scss']
})
export class InternetCheckComponent implements OnInit {

  isConnected: Observable<boolean>;
  sub: Subscription;
  recheckInterval = 30 * 1000;
  pingUrl = 'https://cdnjs.cloudflare.com/ajax/libs/simple-icons/3.0.1/css3.svg?';
  pingInitializedAt = 0;

  constructor() {
    this.isConnected = of(navigator.onLine);
    // this.isConnected = Observable.merge(
    //   Observable.of(navigator.onLine),
    //   Observable.fromEvent(window, "online")..pipe(map(() => true)),
    //   Observable.fromEvent(window, "offline").pipe(map(() => false))
    // );
  }

  refreshStatus(nextSchedule) {
    if (this.sub) {
      this.sub.unsubscribe();
    }
    const timers = timer(1, nextSchedule);
    this.sub = timers.subscribe(t => {
      this.ping();
    });
  }

  ping() {
    if (this.pingInitializedAt) {
      console.log('Already Pinged: ' + this.pingInitializedAt);
      const currentTime = new Date().getTime();
      if (currentTime - this.pingInitializedAt > 14999) {  // ~15 Seconds
        this.isConnected = of(false);
      }
    } else {
      this.pingInitializedAt = new Date().getTime();
      this.pingUrl = this.pingUrl + this.pingInitializedAt;
    }
  }

  forcePing(milliseconds) {   // called from app-component on each successful navigation
    setTimeout(() => {
      this.ping();
    }, milliseconds);
  }

  onSuccess() {
    this.pingInitializedAt = 0;
    this.isConnected = of(true);
  }

  onError() {
    this.pingInitializedAt = 0;
    this.isConnected = of(false);
  }

  @HostListener('window:offline', ['$event'])
  onWindowOffine(event) {
    this.isConnected = of(false);
    this.recheckInterval = 15 * 1000;
    this.refreshStatus(this.recheckInterval);
    // console.log(event);
  }

  @HostListener('window:online', ['$event'])
  onWindowOnline(event) {
    this.isConnected = of(true);
    this.recheckInterval = 30 * 1000;
    setTimeout(() => {
      this.refreshStatus(this.recheckInterval);
    }, 5000);
    // console.log(event);
  }

  ngOnInit() {
    this.refreshStatus(this.recheckInterval);
  }

}
