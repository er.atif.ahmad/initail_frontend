import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { AppConfigService } from 'src/app/services/app-config.service';
import { AuthService } from '../services/auth.service';

import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as fromApp from 'src/app/core/app.states';
import { Store } from '@ngrx/store';
import * as AuthActions from '../store/actions/auth.actions';

import { switchMap, take, map, catchError, filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

/** Pass the request after appending origin url with protocol. */
@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

  private refreshTokenInProgress = false;
  // Refresh Token Subject tracks the current token, or is null if no token is currently
  // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(public appEnv: AppConfigService,private router:Router, private authService: AuthService,
    private store: Store<fromApp.AppState>,private snackBar:MatSnackBar) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {

        if (event instanceof HttpResponse && (event.status / 100) > 3) {
          console.log(`HttpResponse:: Status: ${event.status}, \n event:`, event);
        } else {
          // console.log(`HttpResponse:: Event :`, event);
        }
        return event;
      }),
      catchError((error: any) => {
        if (error instanceof HttpErrorResponse) {

          console.log(`Error HTTP Response: Status: ${error.status}`);
          if (error.status === 403) {
            this.router.navigate(['admin/access-denied'])
            return;
          }
          if (error.status === 503) {
            this.snackBar.open('Import is in progress. Come back after 1 minute...', 'Ok', {
              duration: 2000
            })
            
          }

          // We don't want to refresh token for some requests like login or refresh token itself
          // So we verify url and we throw an error if it's the case
          if (req.url.includes('token/refresh-access-token') || req.url.includes('entrance/login')) {
            // We do another check to see if refresh token failed
            // In this case we want to logout user and to redirect it to login page

            if (req.url.includes('token/refresh-access-token')) {
              this.authService.logout();
            }
            return throwError(error);
          }

          if (error.status !== 401) {
            return throwError(error);
          }

          if (this.refreshTokenInProgress) {
            // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
            // – which means the new token is ready and we can retry the request again
            this.refreshTokenSubject.pipe(
              filter(result => result !== null),
              take(1),
              switchMap(() => {
                return this.sendAuthenticatedRequest(req, next);
              })
            );
          } else {
            this.refreshTokenInProgress = true;
            // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
            this.refreshTokenSubject.next(null);

            // Call auth.refreshAccessToken(this is an Observable that will be returned)
            this.authService.refreshAccessToken().subscribe(
              (tokenData: any) => {
                // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                // for the next time the token needs to be refreshed
                this.refreshTokenInProgress = false;
                this.refreshTokenSubject.next(tokenData.accessToken);

                this.store.dispatch(AuthActions.refreshTokenSuccess(tokenData));

                console.log('Now we have auth token, refreshed', tokenData);
                return this.sendAuthenticatedRequest(req, next);

              },
              (err: any) => {
                this.refreshTokenInProgress = false;

                this.authService.logout();
                return throwError(err);
              }
            );
          }
        }
      }));

  }

  sendAuthenticatedRequest(req, next) {
    console.log('Sending the Pending Requests, Waiting for Access Token Refresh', req);
    return this.store.select('auth').pipe(
      take(1),
      switchMap((authState: fromAuth.State) => {
        if (authState.user && authState.accessToken) {
          const secureReq = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + authState.accessToken)
          });
          return next.handle(secureReq);
        } else {
          return next.handle(req);
        }
      })
    );
  }

}
