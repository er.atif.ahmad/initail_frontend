import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { AppConfigService } from 'src/app/services/app-config.service';
export const SkipBaseUrlHeader = 'X-Skip-Base-Url';


/** Pass the request after appending origin url with protocol. */
@Injectable()
export class CorrectUrlInterceptor implements HttpInterceptor {

  constructor(public appEnv: AppConfigService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (req.headers.has(SkipBaseUrlHeader)) {
      const headers = req.headers.delete(SkipBaseUrlHeader);
      return next.handle(req.clone({ headers }));
    }

    if (req.url.indexOf('http://') > -1 || req.url.indexOf('https://') > -1) {
      return next.handle(req);
    } else {
      const secureReq = req.clone({
        url: this.appEnv.getConfig().origin.concat(req.url),
      });
      return next.handle(secureReq);
    }
  }
}
