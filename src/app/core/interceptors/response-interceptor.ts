import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

/** Pass the request after adding authorization header. */
@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService,private router: Router,private snackBar:MatSnackBar) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && (event.status / 100) > 3) {
          console.log(`HttpResponse:: Status: ${event.status}, \n event:`, event);
        } else {
          // console.log(`HttpResponse:: Event :`, event);
        }
        return event;
      }),
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          console.log(`Error HTTP Response: Status: ${err.status}`);
          if (err.status === 401) {
            this.authService.logout();
          }
          if (err.status === 403) {
            //this.authService.logout();
            this.router.navigate(['/admin/access-denied'])
          }
          if(err.status === 503){
            this.snackBar.open('Import is in progress... Please wait...','Ok',{
              duration:2000
            })
          }
        }
        return throwError(err);
      }));
  }

}


