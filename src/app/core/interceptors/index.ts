/* "Barrel" of Http Interceptors */
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {CorrectUrlInterceptor} from 'src/app/core/interceptors/correct-url-interceptor';
import {AuthorizeRequestInterceptor} from 'src/app/core/interceptors/authorize-request-interceptor';
import { AddJsonHeaderInterceptor } from './add-json-header-interceptor';
import { RefreshTokenInterceptor } from './refresh-token-interceptor';


/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: CorrectUrlInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: AddJsonHeaderInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: AuthorizeRequestInterceptor, multi: true},
  // {provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true},
  {provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true},
];
