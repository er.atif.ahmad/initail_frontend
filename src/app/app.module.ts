import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { UiModule } from './ui/ui.module';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppConfigService } from './services/app-config.service';
import { CoreModule } from './core/core.module';
import { DeviceDetectorService } from 'ngx-device-detector';
import { UniversalDeviceDetectorService } from './universal-device-detector.service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { TranslocoRootModule } from './transloco/transloco-root.module';
import { HttpClientModule } from '@angular/common/http';


const appInitializerFn = (appConfig: AppConfigService) => {
  return () => {
    console.log('Initialing App Config ...');
    return appConfig.loadAppConfig();
  };
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TranslocoRootModule,
    CoreModule,
    SharedModule,
    UiModule,
    NgbModule
  ],
  exports: [RouterModule],
  providers: [
    AppConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [AppConfigService]
    },
    NgxImageCompressService,
    {
      provide: DeviceDetectorService,
      useClass: UniversalDeviceDetectorService
    }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
