import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule, MatMenuTrigger } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRippleModule } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { LayoutModule } from '@angular/cdk/layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatPaginatorModule } from '@angular/material/paginator';

// Added by Shubham
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
//import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
// import { ChartsModule } from 'ng2-charts';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree'
import {MatBadgeModule} from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatSliderModule} from '@angular/material/slider';

//import { ChartsModule, WavesModule } from 'angular-bootstrap-md'

@NgModule({
  declarations: [],
  imports: [
  //  NgxMatSelectSearchModule,
    MatGridListModule,
    MatStepperModule,
    MatTooltipModule,
    MatBottomSheetModule,
    CommonModule, MatMenuModule, MatButtonModule, MatTableModule, MatSidenavModule,
    MatTabsModule, MatToolbarModule, MatIconModule, MatDividerModule, MatCardModule,
    MatFormFieldModule, MatRadioModule, MatSelectModule, MatCheckboxModule, MatInputModule,
    MatSnackBarModule, MatDialogModule, MatRippleModule, MatProgressSpinnerModule,
    MatSlideToggleModule, MatListModule, MatDatepickerModule,
    MatMomentDateModule, MatPaginatorModule, MatChipsModule, MatExpansionModule,
    LayoutModule, // For Breakpoint Observer,
    ScrollingModule, // For cdk-virtual-scroll
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatSliderModule
  ],
  exports: [
    //NgxMatSelectSearchModule,
    //ChartsModule,
    MatGridListModule,
    MatStepperModule,
    MatTooltipModule,
    MatBottomSheetModule,
    CommonModule, MatMenuModule, MatButtonModule, MatTableModule, MatSidenavModule,
    MatTabsModule, MatToolbarModule, MatIconModule, MatDividerModule, MatCardModule,
    MatFormFieldModule, MatRadioModule, MatSelectModule, MatCheckboxModule, MatInputModule,
    MatSnackBarModule, MatDialogModule, MatRippleModule, MatProgressSpinnerModule,
    MatSlideToggleModule, MatListModule, MatDatepickerModule,
    MatMomentDateModule, MatPaginatorModule, MatChipsModule, MatExpansionModule,
    LayoutModule, ScrollingModule,MatButtonToggleModule,
    MatAutocompleteModule,CdkTableModule,CdkTreeModule, MatBadgeModule, MatSliderModule,
    MatMenuTrigger
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class UiModule { }
