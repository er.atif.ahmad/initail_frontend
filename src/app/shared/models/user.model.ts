export class User {
    id: number;
    roleId: number;
    email: string;
    emailVerified: true;
    userName: string;
    name?: string;
    isActive: boolean;
    location?: string;
    gender?: string;
    mobileNo?: string;
    houseId?: string;
    lastLogin?: string;
    roles: string[];
    orgId: string;
    organisationName: string;
    isSuperAdmin?: boolean;
    isShopAdmin?: boolean;
    isShopUser?: boolean;
    isCustomer?: boolean;
    socialAccounts?: any[];
  }