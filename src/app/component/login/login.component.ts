import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AppState } from 'src/app/core/app.states';
import { AuthService } from 'src/app/core/services/auth.service';
import { AppConfigService } from 'src/app/services/app-config.service';
import { AppService } from 'src/app/services/app.service';
import * as AuthActions from 'src/app/core/store/actions/auth.actions';
import * as fromAuth from 'src/app/core/store/reducers/auth.reducers';
import * as fromApp from 'src/app/core/app.states';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('loginForm', { static: false }) loginForm: NgForm;

  invalidLoginMessage = '';
  loadingClass = '';
  organizationDetails;
  defaultOrganizationDetail;
  loaded: boolean = false;
  isForget: boolean = false;
  s3Url;
  loading: boolean = false;

  buttonText = 'Sign In';
  showDefaultLogin: boolean = false;
  showCbreLogin: boolean = false;
  showSensesLogin: boolean = false;
  loginHost;

  display = false;

  usernameText;
  passwordText;


  loginPayload = {
    username: '',
    password: ''
  };

  isInForgetPasswordMode = false;
  /********************var**************/
  deviceInfo = null;
  show = false;
  isMobile = false;
  hide = true;
  /********************var/**************/

  isHandset$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Handset, Breakpoints.TabletPortrait])
    .pipe(
      map(result => {console.log("== result ==", result);
       return result.matches; }),
      shareReplay()
    );

  language = localStorage.getItem('lang');

  themeConfig :any = {
    logos: {
      "banner": {
          "img": "/organizationLogos/ffecf755-3f37-4548-aa19-4be1b0144610.jpg",
          "loading": false
      },
      "logo3": {
          "img": "/organizationLogos/2bd14798-a7b5-425b-83eb-c022f589b67d.png",
          "loading": false
      },
      "logo2": {
          "img": "/organizationLogos/724bb680-8ddf-4cbf-a5c9-a79d80afea71.png",
          "loading": false
      },
      "logo1": {
          "img": "/organizationLogos/875bb0c8-13d6-4092-a808-ed6dddab07bc.png",
          "loading": false
      }
      // logo2: "assets/img/senses-logo.png",
    },
    bgAndFontColor:{
      "header": {
          "text": "#ffffff",
          "font": 'Roboto, "Helvetica Neue", sans-serif',
          "bold": false,
          "bg": [
              "#e26063",
              "#c32124"
          ]
      },
      "dashboard": {
          "text": "#ffffff",
          "font": 'Roboto, "Helvetica Neue", sans-serif',
          "bold": false,
          "inBox": true,
          "bg": [
              "#c32124"
          ]
      },
      "sidebar": {
          "text": "#ffffff",
          "font": 'got_med',
          "bold": false,
          "activeBg": [
            "#e26063"
          ],
          "bg": [
              "#c32124"
          ]
      },
      "default": {
          "font": "OCR A Std, monospace",
          "btnBorder": false,
          "btnType": "",
          "text": "#c32124",
          "primary": "#c32124",
          "heading": "#c32124",
          "icon": "#c32124",
          "bg": [
              "#f1eaeb"
          ],
          "primaryBtn": {
            "text": "#ffffff",
            "bg": [
              "#c32124"
            ],
          },
          "secondaryBtn": {
            "text": "#ffffff",
            "bg": [
              "#e26063"
            ],
          },
          "cancelBtn": {
            "text": "#c32124",
            "bg": [
              "#ffffff"
            ],
          },
          "infoBtn": {
            "text": "#f44239",
            "bg": [
              "#f5fb4d"
            ],
          },
          "warningBtn": {
            "text": "#ffffff",
            "bg": [
              "#f44239"
            ],
          },
          "successBtn": {
            "text": "#ffffff",
            "bg": [
              "#36834f"
            ],
          },
      },
      "notification": {
        "text": "#ffffff",
        "heading": "#ffffff",
        "font": 'Roboto, "Helvetica Neue", sans-serif',
        "bold": false,
        "bg": [
            "#c32124"
        ]
      },
      "account": {
        "text": "#ffffff",
        "font": 'Roboto, "Helvetica Neue", sans-serif',
        "bold": false,
        "bg": [
            "#c32124"
        ]
      },
      "login": {
        "desktop": {
          "page": '1',
          "btnType": "",
          "font": "",
          "radius": true,
          "circleFlag": false,
          "toAll": false,
          "btnText": "#ffffff",
          "heading": "#ffffff",
          "text": "#000000",
          "bg": [
            "#e26063",
            "#c32124"
          ],
          "btnBg": [
            "#e26063",
            "#c32124"
          ]
        },
        "mobile": {
          "page": '1',
          "btnType": "",
          "font": "",
          "radius": true,
          "circleFlag": false,
          "toAll": false,
          "btnText": "#ffffff",
          "heading": "#ffffff",
          "text": "#000000",
          "bg": [
            "#e26063",
            "#c32124"
          ],
          "btnBg": [
            "#e26063",
            "#c32124"
          ]
        },
      }
    }
  }


  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>,
    private snackBar: MatSnackBar,
    // private translocoService: TranslocoService,
    private authService: AuthService, 
    private router: Router,
    private route: ActivatedRoute,
    private appEnv: AppConfigService,
    private appService: AppService
  ) {

    this.loginHost = window.location.host;
    // this.loginHost = "senses.servicemind.asia";
    // this.loginHost = "cbreconnect.servicemind.asia";

    this.s3Url  = this.appEnv.getConfig().bucketUrl;

    this.isHandset$.subscribe(r => {
      console.log('IS HANDSET:', r);
      this.isMobile = r;

      this.show = !this.isMobile;
    });

    this.appService.userThemeConfigData.subscribe(r => {
      this.themeConfig = r;
      // this.theme = 'theme-red';
      this.loaded = true;
      console.log('User Theme From Service:', this.themeConfig);
    });


  }

  ngOnInit(): void {

    this.route.data.subscribe(r =>{
      console.log("route data ===", r);
      if(r?.isForget === true){
        this.isForget = r.isForget;
      }
    });

    console.log(this.isForget);
    
    console.log("=== domain ===", this.loginHost);

    // this.store.select('auth').subscribe((authState: fromAuth.State) => {
    //   if (authState.isAuthenticated) {
    //     this.invalidLoginMessage = '';

    //     console.log("User login Details", authState, "User login Details");

    //     if (authState.user.isCustomer) {
    //       localStorage.setItem('notificationToggle', 'false');
    //       this.router.navigate(['/admin/dashboard']);
    //     }
    //   } else {
        
    //     if(this.language == 'th' && (authState.errorMessage == "Invalid Credentials" || authState.errorMessage == "Invalid credentials")){
    //       this.invalidLoginMessage = "ข้อมูลไม่ถูกต้อง";
    //     }else if(this.language == 'th' && (authState.errorMessage == "Your account is pending for approval. Please contact admin" || authState.errorMessage == "Your account is pending for approval. Please contact admin")){
    //       this.invalidLoginMessage = "รอการอนุมัติบัญชี";
    //     }else if(this.language == 'th' && (authState.errorMessage == "Your Organisation has been Deactivated, Please contact to administration !")){
    //       this.invalidLoginMessage = "โครงการถูกระงับ กรุณาติดต่อเจ้าหน้าที่";
    //     }else if(this.language == 'th' && (authState.errorMessage == "Login for the user of this team is disabled , Please contact to your administration !")){
    //       this.invalidLoginMessage = "ชื่อผู้ใช้นี้ ไม่ได้รับอนุญาติให้เข้าถึงระบบ กรุณาติดต่อเจ้าหน้าที่";
    //     }else if(this.language == 'th' && (authState.errorMessage == "Your account has been blocked, Please contact to administration !")){
    //       this.invalidLoginMessage = "บัญชีผู้ใช้นี้ถูกระงับ กรุณาติดต่อเจ้าหน้าที่";
    //     }else{
    //       this.invalidLoginMessage = authState.errorMessage;
    //     }
    //     this.loadingClass = '';
    //     this.buttonText = 'Sign In';
    //   }
    //   // this.loadingClass = '';
    //   console.log('[InvalidLoginMessage]:', this.invalidLoginMessage);
    // });

  }


  ngAfterViewInit() {
    this.loginForm.valueChanges.subscribe(_ => {
      this.invalidLoginMessage = '';
      console.log('Inside Value Changes')
      // this.loadingClass = '';
    });
  }

  setForgotPasswordMode(mode) {
    if (mode) {
      this.isInForgetPasswordMode = true;
      this.buttonText = 'Recover Password';
    } else {
      this.isInForgetPasswordMode = false;
      this.buttonText = 'Sign In';
    }
  }

  onSubmit(){
    if(this.isForget){
      this.onForgetSubmit();
    }
    else{
      this.onLoginSubmit();
    }
  }

  onLoginSubmit() {

    // if (this.isInForgetPasswordMode) {
    //   this.loadingClass = 'spinning';
    //   this.buttonText = 'Checking';
    //   this.authService.recoverPassword(this.loginPayload).subscribe((r) => {
    //     console.log('Password recovery response:', r);
    //     this.snackBar.open(r.data.message, 'Success', {
    //       duration: 3500,
    //     });
    //     this.isInForgetPasswordMode = false;
    //     this.loadingClass = '';
    //     this.buttonText = 'Sign In';
    //   }, err => {
    //     this.loadingClass = '';
    //     this.buttonText = 'Recover Password';
    //     this.showRelaventErrorPop(err);
    //     console.log('Error occurred: ', err)
    //     this.snackBar.open(err.error.errors[0].message, 'Try Again', {
    //       duration: 2000
    //     })

    //   });

    // }

    if (!this.isInForgetPasswordMode) {
      this.loadingClass = 'spinning';
      this.buttonText = 'Signing In';
      // Will triggered only when form is submitted
      if (this.loginForm.valid) {
        this.loading = true;
        console.log('Form Submitted: values', this.loginPayload);
        this.store.dispatch(AuthActions.login({ email: this.loginPayload.username, password: this.loginPayload.password }));
        // this.loginForm.resetForm();
      } else {
        if (this.language == 'th') {
          this.invalidLoginMessage = 'ระบุ ชื่อผู้ใช้ และ รหัสผ่าน.';
        } else {
          this.invalidLoginMessage = 'Please input username & password.';
        }
        this.loadingClass = '';
        this.buttonText = 'Sign In';
        setTimeout(() => {
          this.invalidLoginMessage = '';
        }, 3000);
      }
    }
  }

  onForgetSubmit() {

    if (this.loginPayload?.username != '') {
      this.loadingClass = 'spinning';
      this.buttonText = 'Signing In';
      this.authService.forgotPassword({email: this.loginPayload?.username ,host:window.location.host}).subscribe(r => {
        this.loading = false;
        this.invalidLoginMessage = r.message;
        this.snackBar.open(r.message, "ok", {
          duration: 4000
        })
        this.loadingClass = '';
        this.buttonText = 'SUBMIT';
        this.loginPayload = {
          username: '',
          password: ''
        };
      }, err => {
        this.invalidLoginMessage='';
        this.loadingClass = '';
        this.buttonText = 'SUBMIT';
        this.loading = false;
        this.snackBar.open(err.error.errors[0].message, "ok", {
          duration: 4000
        })
      })
    } else {
      this.invalidLoginMessage='';
      this.snackBar.open("Enter your valid email address..", "Ok", { duration: 4000 })
    }
  }


  showRelaventErrorPop(httpErr: any) {
    const err = httpErr.error.errors;
    console.log('Relavent Error:', err);
    if (Array.isArray(err) && err.length > 0) {
      console.log('Relavent Error 2222:', err);
      this.snackBar.open(err[0].message, 'Error', {
        duration: 3000,
      });
    } else {
      this.snackBar.open('Oops! Some Error occurred.', 'Error', {
        duration: 3000,
      });
    }
  }

}
